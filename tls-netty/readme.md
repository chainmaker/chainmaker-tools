

## 概述

长安链的国密openssl实现是在`OpenSSL-1.1.1l`的基础上改造的。OpenSSL在 **1.1.1** 版本中引入了国密算法的实现，但是仅仅提供了算法接口，没有把国密算法引入到任何通信协议中去支持。 长安链国密openssl把国密算法的支持加入到 **X509协议** 中。



## 编译说明netty-tcnative

netty-tcnative国密jar包基于长安链国密openssl以及netty-tcnative的[2.0.39.Final](https://github.com/netty/netty-tcnative/tree/netty-tcnative-parent-2.0.39.Final)版本，构建流程参考
netty-tcnative官方地址： [netty-tcnative wiki](https://netty.io/wiki/forked-tomcat-native.html)



## 未来工作

1. 支持 **高性能国密库** 实现：长安链密码算法库目前仅支持tjfoc一种实现，与北大gmssl、腾讯国密库等密码库相比存在数量级上的差距。
2. 支持 **国密双证书** 体系：目前长安链国密TLS仅在国密证书、国密算法SM2和SM3上进行了支持，在与标准国密SSL互联互通上还需要进一步改进和完善。



