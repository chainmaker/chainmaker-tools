package main

import (
	"bytes"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
	"io/ioutil"
)

var (
	path string
	show bool
)

func main() {

	mainCmd := &cobra.Command{
		Use:   "uptool",
		Short: "upgrade tool",
		Long:  "upgrade tool",
		RunE: func(_ *cobra.Command, _ []string) error {
			return updateKey()
		},
	}

	flags := mainCmd.Flags()
	flags.StringVarP(&path, "path", "p", "", "specify storage path")
	flags.BoolVarP(&show, "show", "s", false, "specify show db kv")

	mainCmd.MarkFlagRequired(path)

	mainCmd.Execute()

}

func updateKey() error {
	//遍历打印所有的文件名
	var s []string
	s, _ = GetAllDir(path, s)

	for _, p := range s {
		fmt.Printf("path: %s \n", p)
		//if strings.HasSuffix(p, "/wal") || strings.HasSuffix(p, "\\wal") {
		//	fmt.Println("remove path", p)
		//	os.RemoveAll(p)
		//	continue
		//}
		db, err := leveldb.OpenFile(p, nil)
		if err != nil {
			fmt.Printf("【warn】 path: %s is not leveldb m %s \n", p, err.Error())
			continue
		}
		keyRange := &util.Range{Start: []byte{0x00}, Limit: nil}
		iter := db.NewIterator(keyRange, nil)
		count := 0
		updateCount := 0
		for iter.Next() {
			if show {
				if len(iter.Value()) > 50 {
					fmt.Printf("keyStr:[%s] keyHex:[%x] valueLen:[%d] \n", iter.Key(), iter.Key(), len(iter.Value()))
				} else {
					fmt.Printf("keyStr:[%s] keyHex:[%x] valueLen:[%d] valueStr:[%s] valueHex:[%x] \n", iter.Key(), iter.Key(), len(iter.Value()), iter.Value(), iter.Value())
				}
			} else {
				if bytes.Equal(iter.Key()[:1], []byte{0x00}) {
					db.Put(iter.Key()[1:], iter.Value(), nil)
					updateCount++
				}
			}
			count++
		}
		fmt.Println("kv count", count, " update count", updateCount)
		iter.Release()
	}
	return nil
}

func GetAllFile(pathname string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(pathname)
	if err != nil {
		fmt.Println("read dir fail:", err)
		return s, err
	}
	for _, fi := range rd {
		if fi.IsDir() {
			fullDir := pathname + "/" + fi.Name()
			fmt.Println("fullDir", fullDir)
			s, err = GetAllFile(fullDir, s)
			if err != nil {
				fmt.Println("read dir fail:", err)
				return s, err
			}
		} else {
			fullName := pathname + "/" + fi.Name()
			fmt.Println("fullName", fullName)
			s = append(s, fullName)
		}
	}
	return s, nil
}

func GetAllDir(pathname string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(pathname)
	if err != nil {
		fmt.Println("read dir fail:", err)
		return s, err
	}
	for _, fi := range rd {
		if fi.IsDir() {
			fullDir := pathname + "/" + fi.Name()
			s = append(s, fullDir)
			s, err = GetAllDir(fullDir, s)
			if err != nil {
				fmt.Println("read dir fail:", err)
				return s, err
			}
		}
	}
	return s, nil
}
