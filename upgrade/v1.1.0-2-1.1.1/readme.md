## 使用方式

### 直接使用

提供了linux系统x86-64的二进制文件，其他可自行编译

```sh
# 1、下载upgrade二进制文件
# 2、执行，其中path为chainmaker.yml文件中storage.store_path值得目录的绝对路径，或相对当前路径。

# 如： chainmaker.yml在此目录/data/build/release/wx-org1/config/wx-org1.chainmaker.org/chainmaker.yml且storage.store_path = ../data/ledgerData1
# 则写如下路径：
upgrade -p /data/build/release/wx-org1/data/ledgerData1
# 或者
cd /data/build/release/wx-org1/data/ledgerData1
upgrade -p .
```



### 先编译再使用

```sh
# 编译upgrade二进制文件
go build
# 按照上一章节使用即可
```

